import sys, copy
import numpy as np

class Save(object):
    def __init__(self, grid, cell_options, arg_min):
        self.grid = copy.deepcopy(grid)
        self.cell_options = copy.deepcopy(cell_options)
        self.arg_min = copy.deepcopy(arg_min)
        self.last_choice = 0

class Sudoku(object):
    def __init__(self, in_):
        self.grid = np.zeros((9, 9), dtype=int)
        self.cell_options = [[set(range(1, 10)) for j in range(9)] for i in range(9)]
        self.choices = []
        self.cells_tried = []

        self.debug = False

        self.read(in_)

    def __repr__(self):
        repr_str = []
        for i in range(9):
            str_list = []
            for j in range(9):
                if self.grid[i, j] == 0:
                    str_list.append('.')
                else:
                    str_list.append(str(self.grid[i, j]))
                str_list.append(' ')
                if j!= 8 and j%3 == 2: str_list.append('| ')
            repr_str.append(''.join(str_list))
            if i != 8 and i%3 == 2:
                repr_str.append('\n')
                repr_str.append('-'*21)
            repr_str.append('\n')

        return ''.join(repr_str)

    def setAnswer(self, i, j, v):
        self.grid[i, j] = v
        self.cell_options[i][j] = {v}

    def read(self, in_):
        with open(in_, 'r') as f:
            for i, line in enumerate(f):
                for j, c in enumerate(line):
                    try:
                        v = int(c)
                        self.setAnswer(i, j, v)
                    except ValueError:
                        pass

    def updateFounds(self, v, founds):
        if v != 0:
            if v in founds:
                raise ValueError
            founds.add(v)

    def updateOptionsLine(self, i):
        founds = set()
        for j in range(9):
            self.updateFounds(self.grid[i, j], founds)

        return set(range(1, 10)) - founds

    def updateOptionsCol(self, j):
        founds = set()
        for i in range(9):
            self.updateFounds(self.grid[i, j], founds)

        return set(range(1, 10)) - founds

    def updateOptionsSquare(self, top_left_i, top_left_j):
        start_i = top_left_i
        end_i = start_i + 3

        start_j = top_left_j
        end_j = start_j + 3

        founds = set()
        for l in range(start_i, end_i):
            for c in range(start_j, end_j):
                self.updateFounds(self.grid[l, c], founds)

        return set(range(1, 10)) - founds

    def updateOptions(self):
        options_line = {0: None, 1: None, 2: None,
                        3: None, 4: None, 5: None,
                        6: None, 7: None, 8: None}

        options_col = {0: None, 1: None, 2: None,
                       3: None, 4: None, 5: None,
                       6: None, 7: None, 8: None}

        options_square = {0: {0: None, 1: None, 2: None},
                          1: {0: None, 1: None, 2: None},
                          2: {0: None, 1: None, 2: None}}

        for i in range(9):
            options_line[i] = self.updateOptionsLine(i)
        for j in range(9):
            options_col[j] = self.updateOptionsCol(j)

        for i in range(3):
            for j in range(3):
                options_square[i][j] = self.updateOptionsSquare(3 * i, 3 * j)

        for i in range(9):
            for j in range(9):
                if self.grid[i, j] == 0:
                    self.cell_options[i][j] = \
                        options_line[i] & options_col[j] & options_square[i//3][j//3]
                else:
                    self.cell_options[i][j] = {self.grid[i, j]}

    def uniqueOptionSquare(self, i, j):
        start_i = 3 * (i//3)
        end_i = start_i + 3

        start_j = 3 * (j//3)
        end_j = start_j + 3

        s = set()
        for l in range(start_i, end_i):
            for c in range(start_j, end_j):
                if not (l == i and c == j):
                    s = s | self.cell_options[l][c]
                    if self.debug: print('s opt', l, c, self.cell_options[l][c], s)

        return self.cell_options[i][j] - s

    def uniqueOptionLine(self, i, j):
        s = set()
        for c in range(9):
            if c != j:
                s = s | self.cell_options[i][c]
                if self.debug: print('l opt', i, c, self.cell_options[i][c], s)

        return self.cell_options[i][j] - s

    def uniqueOptionCol(self, i, j):
        s = set()
        for l in range(9):
            if l != i:
                s = s | self.cell_options[l][j]
                if self.debug: print('c opt', l, j, self.cell_options[l][j], s)

        return self.cell_options[i][j] - s

    def uniqueOption(self, i, j):
        unique_option_col = self.uniqueOptionCol(i, j)
        unique_option_line = self.uniqueOptionLine(i, j)
        unique_option_square = self.uniqueOptionSquare(i, j)

        ret = None
        for chunk in [unique_option_col, unique_option_line, unique_option_square]:
            if len(chunk) == 1:
                if ret and ret != list(chunk)[0]: raise ValueError
                ret = list(chunk)[0]

        return ret

    def simpleSolve(self):
        stop = False

        while not stop:
            self.updateOptions()
            count = 0

            for i in range(9):
                for j in range(9):
                    if self.grid[i, j] == 0 and len(self.cell_options[i][j]) == 1:
                        self.setAnswer(i, j, list(self.cell_options[i][j])[0])
                        count += 1

            for i in range(9):
                for j in range(9):
                    if self.grid[i, j] == 0 and len(self.cell_options[i][j]) > 1:
                        if self.debug:
                            print()
                            print(i, j)

                        ret = self.uniqueOption(i, j)
                        if ret:
                            self.setAnswer(i, j, ret)
                            count += 1
            if self.debug:
                print(self)
                _ = input()

            if count == 0:
                stop = True

    def makeChoice(self):
        arg_min = None
        min_options = 9
        for i in range(9):
            for j in range(9):
                if 1 < len(self.cell_options[i][j]) and \
                   len(self.cell_options[i][j]) < min_options and \
                   not (i, j) in self.cells_tried:
                    arg_min = (i, j)
                    min_options = len(self.cell_options[i][j])

        if arg_min is not None:
            self.cells_tried.append(arg_min)
            self.choices.append(Save(self.grid,
                                     self.cell_options,
                                     arg_min))
        else:
            i_choice, j_choice = self.choices[-1].arg_min
            while self.choices[-1].last_choice + 1 >= len(self.cell_options[i_choice][j_choice]):
                _ = self.choices.pop()
                self.cell_options = copy.deepcopy(self.choices[-1].cell_options)
                i_choice, j_choice = self.choices[-1].arg_min

            self.grid = copy.deepcopy(self.choices[-1].grid)
            self.choices[-1].last_choice += 1

        i_choice, j_choice = self.choices[-1].arg_min
        self.setAnswer(i_choice, j_choice, list(self.cell_options[i_choice][j_choice])[self.choices[-1].last_choice])

    def solve(self):
        solved = False
        while not solved:
            try:
                self.simpleSolve()
                solved = not np.isin(0, self.grid)
            except ValueError:
                if self.debug: print('[ERROR] Stopped on value error')

            if not solved:
                self.makeChoice()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        sudoku = Sudoku(sys.argv[1])
        # sudoku.debug = True
        sudoku.solve()

        print("====== Result ======")
        print(sudoku)
