# Sudoku Solver

A simple and not optimised (speed nor memory usage) sudoku solver.

See the `sudokus` folder for input examples.

## Dependencies

* Python 3
* numpy

## Usage

```
$ python3 main.py sudokus/s0.txt
====== Result ======
1 2 3 | 4 5 6 | 7 8 9
4 5 6 | 7 8 9 | 1 2 3
7 8 9 | 1 2 3 | 4 5 6
---------------------
2 3 4 | 5 6 7 | 8 9 1
5 6 7 | 8 9 1 | 2 3 4
8 9 1 | 2 3 4 | 5 6 7
---------------------
3 4 5 | 6 7 8 | 9 1 2
6 7 8 | 9 1 2 | 3 4 5
9 1 2 | 3 4 5 | 6 7 8
```
